import '@testing-library/jest-dom/extend-expect';
import * as React from 'react'
import '@testing-library/jest-dom'
import { MockedProvider } from '@apollo/client/testing';  
import {render, screen} from '@testing-library/react'
 
import ShipComponent from '@/app/components/ship';
import { Ship } from '@/app/queries/ships';
 
 
const mockShip: Ship = {
    id: '1',
    model: 'Model 1',
    name: 'Ship 1',
    type: 'Type 1',
    status: 'Active',
  };
  
  const renderWithApolloMock = () => {
    return render(
      <MockedProvider>
        <ShipComponent {...mockShip} />
      </MockedProvider>
    );
  };
  
  describe('ShipComponent', () => {
    it('renders ship details correctly', async () => {
      renderWithApolloMock();
  
      expect(screen.getByText(mockShip.name)).toBeInTheDocument();
      expect(screen.getByText(`Model: ${mockShip.model}`)).toBeInTheDocument();
      expect(screen.getByText(`Type: ${mockShip.type}`)).toBeInTheDocument();
      expect(screen.getByText(`Status: ${mockShip.status}`)).toBeInTheDocument();
      expect(screen.getByText(`Ship ID: ${mockShip.id}`)).toBeInTheDocument();
    });
  });