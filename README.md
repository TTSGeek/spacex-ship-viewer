 

# SpaceX Ship Viewer

This web application allows you to view details of SpaceX ships retrieved from the SpaceX GraphQL API.

## Features

- Displays a list of SpaceX ships with details such as name, model, type, and status.
- Supports pagination to load more ships.
- Skeleton loading while data is being fetched.
- Error handling for failed data retrieval.

## Technologies Used

- React.js
- Next.js
- Tailwind CSS
- Apollo Client for GraphQL integration

## Setup

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/TTSGeek/spacex-ship-viewer.git
   ```
2. Navigate to the project directory:
   ```bash
   cd spacex-ship-viewer
   ```
3. Install dependencies:
   ```bash
   npm|yarn install
   ```
4. Start the development server:
   ```bash
   npm|yarn run dev
   ```


Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
 