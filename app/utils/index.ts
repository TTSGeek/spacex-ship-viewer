import React from "react";
import { ReactElement } from "react";

export function DuplicateComponent(children: ReactElement, x: number): ReactElement[] {
    const components: ReactElement[] = [];
    for (let i = 0; i < x; i++) {
      components.push(React.cloneElement(children, { key: i }));
    }
    return components;
  }