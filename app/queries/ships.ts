import { gql } from '@apollo/client';

export const GET_SHIPS = gql`
  query GetShips($limit: Int!, $offset: Int!) {
    ships(limit: $limit, offset: $offset) {
      id
      model
      name
      type
      status
    }
  }
`;

export interface Ship {
  id: string;
  model: string;
  name: string;
  type: string;
  status: string;
}

export interface PaginatedShips {
  ships: Ship[];
}