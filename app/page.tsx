"use client"
 

import { useQuery } from '@apollo/client';
import { GET_SHIPS, Ship, PaginatedShips } from './queries/ships';
import { useEffect, useState } from 'react';
import { Button } from "@/components/ui/button";
import ShipComponent from "./components/ship";
import { Skeleton } from "@/components/ui/skeleton";
import { DuplicateComponent } from "./utils";
import { ReloadIcon } from "@radix-ui/react-icons";
import { cn } from "@/lib/utils";
import { INITIAL_SHIP_OFFSET, SHIPS_PER_PAGE } from './const';

export default function Page() {

  const [ships, setShips] = useState<Ship[]>([]);

  const [offset, setOffset] = useState(INITIAL_SHIP_OFFSET);

  const [loadingMore, setLoadingMore] = useState(false);

  const { loading, error, data, fetchMore } = useQuery<PaginatedShips>(GET_SHIPS, {
    variables: { limit: SHIPS_PER_PAGE, offset },
  });

  useEffect(() => {
    if (data && data.ships) {
      setShips((prevShips) => [...prevShips, ...data.ships]);
    }
  }, [data]);

  const handleLoadMore = () => {
    setLoadingMore(true);
    fetchMore({
      variables: { offset: data?.ships.length },
    }).then((fetchMoreResult) => {
      setOffset(fetchMoreResult.data.ships.length);
      setLoadingMore(false);
    });
  };


  if (error)   throw new Error(error.message);


  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className=" w-ful flex flex-col max-w-5xl items-center justify-between font-mono text-sm  ">

        <h1 className="text-3xl font-bold text-center mb-8">SpaceX Ships</h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
          {loading && ships.length === 0 ? (
            DuplicateComponent((
              <div className="flex flex-col space-y-3">
                <Skeleton className="h-[125px] w-[250px] rounded-xl" />
                <div className="space-y-2">
                  <Skeleton className="h-4 w-[250px]" />
                  <Skeleton className="h-4 w-[200px]" />
                </div>
              </div>
            ), 5)

          ) : (
            ships.map((ship: Ship) => (
              <ShipComponent key={ship.id} {...ship} />
            ))
          )}
        </div>

        <Button
          variant={"ghost"}
          disabled={loadingMore}
          onClick={handleLoadMore}
          className={cn("mt-5", { "cursor-not-allowed opacity-50": loadingMore })}
        >
          {loadingMore ? (
            <>
              <ReloadIcon className="mr-2 h-4 w-4 animate-spin" />
              Please wait
            </>
          ) : (
            'Load More'
          )}
        </Button>

      </div>


    </main >
  );
}
