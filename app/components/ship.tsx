import { Ship } from "../queries/ships";
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card";

const ShipComponent = (props: Ship) => {
    const { id, model, name, type, status } = props;

    return (
        <Card key={id}>
            <CardHeader>
                <CardTitle>{name}</CardTitle>
                <CardDescription>{type}</CardDescription>
            </CardHeader>
            <CardContent>
                <p>Model: {model}</p>
                <p>Status: {status}</p>
            </CardContent>
            <CardFooter>
                <p>Ship ID: {id}</p>
            </CardFooter>
        </Card>
    );
};

export default ShipComponent;