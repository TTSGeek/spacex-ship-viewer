import { Button } from '@/components/ui/button' 
import React, { Component, ReactNode } from 'react'
 

type ErrorBoundaryState = {
  hasError: boolean
}

type ErrorBoundaryProps = {
    children : ReactNode
}

export default class AppErrorBoundary extends Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  state = { hasError: false }

  static getDerivedStateFromError(error: any) {
  
    return { hasError: true }
  }

  render() {
    if (this.state.hasError) {
      return (
        <div
          className="p-0 h-[100vh] relative flex justify-center items-center"
          
        >
          <div
     
            className='p-6 space-y-8 shadow-lg inline items-center bg-korboo-header'
          >
             
            
            <div>
              <b>Oups…</b>
              <br />
              Something went wrong! Clear cache and refresh.
              <Button
                onClick={() => { 
                  window.location.reload()
                }}
                variant="outline" 
                size="sm"
                className='mt-4 block' 
              >
                Reload
              </Button>
            </div>
          </div>
        </div>
      )
    }

    return this.props.children
  }
}
